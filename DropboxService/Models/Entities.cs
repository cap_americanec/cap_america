﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DropboxService.Models
{
    public abstract class Entities
    {
        public bool IsChecked { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Icon { get; set; }
        public DateTime Modified { get; set; }
    }

    public class Files : Entities
    {
        public string Size { get; set; }
    }

    public class Folders : Entities
    {

    }

    public class EntityViewModel
    {
        public string Path { get; set; }
        public List<Files> Files { get; set; }
        public List<Folders> Folders { get; set; }
    }
}