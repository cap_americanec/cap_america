﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DropNet;
using DropboxService.Models;

namespace DropboxService.Controllers
{
    public class HomeController : Controller
    {
        #region login
        public ActionResult Login()
        {
            DropNetClient client = new DropNetClient("com906mjg1nx6jk", "76gfcidzzn1vmdv");

            string url = client.GetTokenAndBuildUrl("http://localhost:58271/Home/Confirm");

            Session["token"] = client.UserLogin.Token;
            Session["secret"] = client.UserLogin.Secret;

            ViewBag.Url = url;

            return View();
        }

        public ActionResult Confirm()
        {
            if (Session["token"] == null && Session["secret"] == null)
            {
                return RedirectToAction("Login");
            }

            string token = Session["token"].ToString();
            string secret = Session["secret"].ToString();

            DropNetClient client = new DropNetClient("com906mjg1nx6jk", "76gfcidzzn1vmdv", token, secret);

            var accessToken = client.GetAccessToken();

            Session["client"] = client;

            return RedirectToAction("Index");
        }
        #endregion

        public ActionResult Index()
        {
            if (Session["client"] == null)
            {
                return RedirectToAction("Login");
            }

            DropNetClient client = (DropNetClient)Session["client"];
            var listOfFolders = new List<Folders>();
            var listOfFiles = new List<Files>();
            var data = client.GetMetaData(path: "/");

            try
            {
                data.Contents.ForEach(entity =>
                {
                    if (entity.Is_Dir)
                    {
                        listOfFolders.Add(new Folders()
                        {
                            Icon = entity.Icon,
                            Name = entity.Name,
                            Path = entity.Path,
                            Modified = entity.ModifiedDate
                        });
                    }
                    else
                    {
                        listOfFiles.Add(new Files()
                        {
                            Icon = entity.Icon,
                            Name = entity.Name,
                            Path = entity.Path,
                            Size = entity.Size,
                            Modified = entity.ModifiedDate
                        });
                    }
                });
            }
            catch (Exception)
            {
                RedirectToAction("Login");
            }

            EntityViewModel evm = new EntityViewModel();
            evm.Path = data.Path;
            evm.Folders = listOfFolders;
            evm.Files = listOfFiles;
            return View(evm);
        }

        public ActionResult BrowseInFolder(string filePath)
        {

            if (Session["client"] == null)
            {
                return RedirectToAction("Login");
            }

            DropNetClient client = (DropNetClient)Session["client"];
            var listOfFolders = new List<Folders>();
            var listOfFiles = new List<Files>();

            try
            {
                var metaData = client.GetMetaData(path: filePath);

                if (!metaData.Is_Dir)
                {
                    var url = client.GetShare(filePath);
                    return Redirect(url.Url);
                }
                metaData.Contents.ForEach(entity =>
                {
                    if (entity.Is_Dir)
                    {
                        listOfFolders.Add(new Folders()
                        {
                            Icon = entity.Icon,
                            Name = entity.Name,
                            Path = entity.Path,
                            Modified = entity.ModifiedDate
                        });
                    }
                    else
                    {
                        listOfFiles.Add(new Files()
                        {
                            Icon = entity.Icon,
                            Name = entity.Name,
                            Path = entity.Path,
                            Size = entity.Size,
                            Modified = entity.ModifiedDate
                        });
                    }
                });
            }
            catch (Exception)
            {
                RedirectToAction("Login");
            }

            EntityViewModel evm = new EntityViewModel();
            evm.Path = filePath;
            evm.Files = listOfFiles;
            evm.Folders = listOfFolders;
            return View("Index", evm);
        }

        [HttpPost]
        public ActionResult CreateFolder(string folderName, string filePath)
        {
            if (Session["client"] == null)
            {
                return RedirectToAction("Login");
            }

            DropNetClient client = (DropNetClient)Session["client"];

            if (folderName != null)
            {
                try
                {
                    var metaData = client.CreateFolder(filePath + "/" + folderName);
                    return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
                }
                catch (DropNet.Exceptions.DropboxRestException)
                {
                    return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
                }
            }
            return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
        }
    }
    
}
